# Cours ansible
par Rainald Durand

## CI/CD

Une chaîne de déploiment continue a été mis en place afin de déployer le contenu de se projet dans le serveur Azure.

## Déployer les containers Targets
Au redémarrage de la machine Azure, les containers Targets doivent être redémarrées.
Il y a également des opérations à faire une fois démarrer, ce qui donnait lieu d'écrire un playbook pour cette tâche redondante.

Démarrer les containers : 

    ansible-playbook init-docker-container/main-playbook.yml -t create_container -e docker_container_name=<nom_container>

Ce playbook ne configure pas le fichier /etc/hosts, qui est essentiel pour pouvoir contacter les containers par une résolution DNS

**@TODO:**
    - modifier le playbook init-docker-container pour ajouter la possibilité d'exposer les ports du container