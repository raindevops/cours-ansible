# Rôle : Apache

Ce rôle permet d'installer et de démarrer apache.

Ce rôle permet de :
- Installer apache
- Désinstaller apache
- Upload un fichier de configuration et redémarrer les services apache
- Démarrer les services apache

## Tags
- apache: cible tout le rôle depuis le playbook
- install: Installe apache
- uninstall: Désintalle apache
- install-apache / uninstall-apache: Installe / désinstalle seulement apache

## Variables
- uninstall_apache: false par défaut, set to true pour désinstaller apache

## Dépendances

Pas de dépendances