# Rendu noté du cours ansible

## Prérequis

### Build les images docker

**Image du server web**

    docker build -t ubuntu1804_web ./Dockerfiles/ubuntu1804_web


**Image du server db**

    docker build -t ubuntu1804_db ./Dockerfiles/ubuntu1804_db

### Docker run

**Run le container host_web**

    docker run --rm -dti --name=host_web -p 80:80 ubuntu1804_web

**Run le container host_db**

    docker run --rm -dti --name=host_db -p 3306:3306 ubuntu1804_db

### Ajouter l'ip des containers dans le fichier /etc/hosts

 **récupérer l'ip des containers** 

    docker inspect host_web : regarder le champ IPaddress
    docker inspect host_db : regarder le champ IPaddress

Récupérer la valeur dhu champ IPaddress et l'ajouter dans le fichiers /etc/hosts:
exemple : 

    172.17.0.2 host_web
    172.17.0.3 host_db

### Établir la connexion SSH avec les containers

    ssh-copy-id -i /root/.ssh/id_rsa.pub root@<ip_container>

**vérifier la connexion**

    ssh root@<ip_container>

## Commandes

Run all the playbook

    ansible-playbook -i hosts  main-playbook.yml --diff --ask-vault-pass


Run apache role

    ansible-playbook -i hosts  main-playbook.yml -t apache --diff --ask-vault-pass

Run php role

    ansible-playbook -i hosts  main-playbook.yml -t php --diff --ask-vault-pass

Run mysql role

    ansible-playbook -i hosts  main-playbook.yml -t mysql --diff --ask-vault-pass

Run install tasks from all  roles

    ansible-playbook -i hosts  main-playbook.yml -t install --diff --ask-vault-pass

Upload all files

    ansible-playbook -i hosts  main-playbook.yml -t upload --diff --ask-vault-pass

Curl apache server:

    ansible-playbook -i hosts  main-playbook.yml -t debug --diff --ask-vault-pass
