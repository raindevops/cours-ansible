# Déploiement d'un projet Lamp


## Commandes

### Docker

**Run docker containers**

    ansible-playbook main-playbook.yml -t container -e docker_container_name=host_db -e docker_container_port="3306:3306" -e docker_container_image="ubuntu_db" --ask-vault-pass

    ansible-playbook main-playbook.yml -t container -e docker_container_name=host_web -e docker_container_port="80:80" --ask-vault-pass

**Vérifier la connexion**

    ansible -i inventory/hosts <group_in_inventory_file> -m ping

Si un soucis de connexion persiste : 

    ssh-keygen -f "/root/.ssh/known_hosts" -R "<host_name || host ip>"

**Build les images**

    ansible-playbook main-playbook.yml -t build --ask-vault-pass

### Install apache

    ansible-playbook -i inventory/hosts main-playbook.yml -t apache --ask-vault-pass

### Install mysql

    ansible-playbook -i inventory/hosts main-playbook.yml -t mysql --ask-vault-pass
