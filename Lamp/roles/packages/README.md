# Role: packages

This role install requires package on target host

## Tags

- install: This tag is commonly use with other tasks in other role. Its allows you to configure hosts.
- install-pip2: This tag allow you to install pip2

## Variables

- pip_installation_script_url: Url use to donwload pip2.