# Rôle : docker

Ce rôle permet de : 

- Installer docker
- Installer docker-compose
- Build des images dockers
- Lancer les containers avec docker-compose | docker run

## Tags

- docker: cible tout le rôle
- install: installe docker et docker-compose
- build: Build les images docker
- compose: Upload docker-compose et lance les containers
- container: Lance un container avec docker run, et configure la connexion SSH
- upload-compose: Upload le fichier docker-compose
- run-compose: lance les containers avec docker-compose

## Variables

### Install docker
- docker_install_state: status du packages apt Docker
- pip_packages_docker: Liste des packages pip requis

### Docker-compose

- docker_compose_version: Version de docker-compose
- docker_compose_install_url: lien vers la source d'installation de docker-compose
- docker_compose_install_dest: Destination sur l'hôte du fichier d'installation
- docker_compose_install_mode: chmod du fichier d'installation 
- enable_compose: True: lance les containers avec docker-compose | False : lance les containers avec docker run
- docker_compose_file_path: Chemin vers le docker-compose.yml
- docker_compose_network: Nom du network
- docker_compose_db_vol: Nom du volume
- docker_compose_state: status des containers (present, absent, restarted ...)

### Build docker image

- docker_image_build_data
    - key: nom de l'image à build
        - tag: tag de l'image
        - name: nom de l'image
        - path: chemin vers le Dockerfile
        - filename: nom du Dockerfile
        - container name: nom du container 
 

## Dépendances

- rôle: packages, tags: install-pip2 