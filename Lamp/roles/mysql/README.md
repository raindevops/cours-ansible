# Role: mysql

Ce rôle permet de :

- Installer les dépendances à mysql
- Installer Mysql
- Upload le fichier de configuration
- Creer une base de donnée


## Tag

- init-mysql: init mysql env
- configurate-mysql: upload template & create database

## Variables

- mysql_user: Mysql root user
- mysql_password: Mysql root password
- mysql_host_database_name: Nom de la base de donnée à créer
