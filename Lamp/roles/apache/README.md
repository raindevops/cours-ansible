# Role: apache

Ce rôle permet de : 

- Installer apache
- Upload le fichier de conf
- Upload le fichier index.php et supprimer index.html
- Upload le fichier phpinfo.php

## Tags
- install-php: Install php using apt
- upload-conf: upload le fichier de configuration apache
- upload-file: upload le fichier index.php
- upload-info: upload le fichier phpinfo.php

## Vars

TODO : add variables