# Rôle: Apache

Ce rôle installe apache sur la machine distante

Il permet de : 

- Installer le package apache suivant l'os de la machine
- Démarrer le service apache
- Déployer un fichier index.html templaté avec Jinja
- Redémarrer le service

**Tags associés**

- upload-file: déployer le fichier index.html templaté
- apache: Lance tout le rôle apache.
- create-service-account : crée un compte de service
- debug-user: vérifie que le compte de service soit bien créé/supprimé

**Variables assocciées**

- web_user: Nom de l'utilisateur à afficher dans le site web. Située dans ./defaults/main.yml