# Rôle: User

Ce rôle manipule des utilisateurs dans la machine distante.

Il permet de : 

- Créer un utilisateur
- Permettre la connexion SSH avec cette utilisateur
- Vérifier que l'utilisateur soit bien créer
- Supprimer cette utilisateur
- Vérifier la suppression de l'utilisateur
- Supprimer ce compte de service

**Tags associés**

- create : crée un utilisateur
- delete: supprimer un utilisateur
- delete-service-account : supprime un compte de service

**Variables assocciées**

- default_apache_user: nom de l'utilisateur. Située dans /vars/secrets.yml
- default_apache_password: mot de passe de l'utilisateur. Située dans /vars/secrets.yml