# TP5 :  créer un serveur apache, déployer un fichier HTML custom

Ce playbook est configuré pour :

- ajouter un utilisateur 
- supprimer un utilisateur
- ajouter un compte de service
- supprimer un compte de service
- installer Apache suivant la famille d'OS (Debian vs Redhat)
- Déployer un fichier index.html templaté via Jinja2
- Vérifier le bon fonctionnement du server Apache

## Commandes 

**Ajouter un utilisateur**

    ansible-playbook -i hosts main.yml --ask-vault-pass -t create --diff

**Supprimer un utilsiateur**

    ansible-playbook -i hosts main.yml --ask-vault-pass -t delete --diff

La variable *_-e default_apache_user_* permet de modifier le nom de l'utilisateur à créer  
La variable *_-e default_apache_password_* permet de modifier le mot de passe de l'utilisateur à créer

**Ajouter un compte de service**

    ansible-playbook -i hosts main.yml --ask-vault-pass -t create-service-account

**Supprimer un compte de service**

    ansible-playbook -i hosts main.yml --ask-vault-pass -t delete-service-account

**Vérifier la liste des utilisateurs**

    ansible-playbook -i hosts main.yml --ask-vault-pass -t debug-user

**Installer Apache et déployer le fichier index.html**

    ansible-playbook -i hosts main.yml --ask-vault-pass -t apache --diff

**Déployer le fichier**

    ansible-playbook -i hosts main.yml --ask-vault-pass -t upload-file --diff

La variable *_-e web_user_* permet de modifier le nom dans index.html

**Vérifier le bon fonctionnement d'Apache**

    ansible-playbook -i hosts main.yml --ask-vault-pass -t debug-apache --diff

**Lancer tout le playbook sans supprimer les utilsateurs**

    ansible-playbook -i hosts main.yml --ask-vault-pass --skip-tags="delete, delete-service-account" --diff