# Rôle : create-user

Ce rôle permet de creer des utilisateurs dans les machines distantes.
Il se connecte en ssh aux hôtes, et vérifie que les utilisateurs soient bien créés avec la command *_whoami_*

Ce rôle crée :
 - un utilisateur
 - un mot de passe
 - un répertoire associé dans /home
 - définit /bin/bash comme shell par défaut

## Commandes 

Creer des utilisateurs: 

    ansible-playbook -i hosts main.yml --ask-vault-pass -t create-user

Le nom et le mot de passe des utilisateurs sont contenus dans /vars/secrets.yml, protégés par Ansible Vault
