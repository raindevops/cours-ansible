# Rôle : apt-packages

Ce rôle permet d'installer ou de désinstaller les pacakges décrit dans la variable *_pacakges_*

## Commandes 

Installer : 

    ansible-playbook -i hosts main.yml --ask-vault-pass -t install-packages

Désinstaller :

    ansible-playbook -i hosts main.yml --ask-vault-pass -t uninstall-packages -e uninstall=true